# README #

### What is this repository for? ###

* This is the docker image used within ECS containers to dynamically provision object-instances in AWS ECS. This docker image could be built into AWS ECR docker repository using a `dind` based `AWS CodeBuild` project setup as below. 

### How do I get set up? ###

#### Step 1 - Setting up AWS ECR (Elastic Container Registry) ####

1. Login to AWS. Go to AWS `ECR`. Click in `Create Repository`. Provide an appropriate `Repository name` (e.g. in this case let's call it `object/object-deploy`). (Make sure to create the AWS `ECR repository` in the `same AWS region` in which you're planning to setup `AWSCodeBuild` project)
2. Grab the repository url of the new repository you just created which will be in the format: `$AWS_ACCOUNT_ID.dkr.ecr.$AWS_DEFAULT_REGION.amazonaws.com/$IMAGE_REPOSITORY_NAME`
3. Keep track of the values of `$AWS_ACCOUNT_ID` (a long number, exactly the same number displayed as your AWS Account Number in the AWS Support Page), `$AWS_DEFAULT_REGION` (region you created the ECR repository, such as `ap-southeast-2`) and `$IMAGE_REPOSITORY_NAME` (such as `object/object-deploy`) as they will be required in the latter steps.

#### Step 2 - Setting up Required AWS IAM Access Roles ####

1. To create the required AWS access roles, go to AWS `IAM` (Identity and Access Management). Go to `Roles`. Click on `Create Role`. Then for `Trusted Entity` select `AWS Service` and from AWS Services select `CodeBuild`. 
2. Click on `Next`. Go to `Permissions`. Select `AWSCodeBuildAdminAccess`, `AmazonS3FullAccess`, `CloudWatchFullAccess` and `AmazonEC2ContainerRegistryFullAccess`. Click on next steps and in the review page, make sure you have `AWSCodeBuildAdminAccess`, `AmazonS3FullAccess`, `CloudWatchFullAccess` and `AmazonEC2ContainerRegistryFullAccess` are attached as roles.
3. Give an appropriate role name. (e.g. `ObjectAWSDindCodeBuildRole`)

#### Step 3 - AWSCodeBuild Project Setup - Project Configuration and Environment Sections ####

1. Go to AWS `Code Build`. Click on `Create Project`.
2. Under `Project Configuration` - Provide an appropriate project name (e.g. `object-deployment-docker-build`) and description. Leave other settings in the section as default.
3. Under `Source` -> `Source 1 - Primary`; select `BitBucket` as the source provider. Enter your Bitbucket credentials when prompted. Then select `this` repository as the `Repository`. Leave other settings in the `Source` section as default.
4. In the `Environment` Section provide the below settings. 

Setting  | Value
------------- | -------------
Environment Image  | Select `Managed Image` option
Operating System  | Select `Ubuntu`
Runtime  | `Docker`
Runtime Version  | `aws/codebuild/docker:18.09.0`
Service Role  | Select `Existing Service Role` 
Role name  | Select Service Role created during `Step 2` (i.e. `ObjectAWSDindCodeBuildRole`)
Additional Configuration/Compute   | Select `7 GB memory, 4 vCPUs`

Add the below `Environment Variables` with below predefined values. 

Environment Variable  | Value  | Type
------------- | ------------- | -------------
AWS_DEFAULT_REGION  | Region you created AWS ECR Repository during `Step 1` (e.g. `ap-southeast-2`) | plaintext
AWS_ACCOUNT_ID  | Your AWS Account ID obtained during `Step 1` | plaintext
IMAGE_REPO_NAME  | `Repository name` given during creating AWS ECR repo during `Step 1` (e.g. `object/object-deploy`) | plaintext
IMAGE_TAG  | `latest` | plaintext

#### Step 4 - AWSCodeBuild Project Setup - Buildspec, Artifacts and Logs Section ####

1. Under `Buildspec` -> `Build specifications`, select `Use a buildspec file` and leave `Buildspec name` text box as empty. This will let AWSCodeBuild to build using the `buildspec.yml` file which is found within the root folder of this source repository.
2. Leave with the default settings in `Artifacts` and `Logs` Sections.
3. Finally click `Create build project` button to create the AWSCodeBuild project.

#### Step 5 - AWSCodeBuild Project Setup - Running the Build Project to Create Docker Image ####

1. In AWS `CodeBuild` go to `Build projects` link.
2. Click on the build project you created during `Steps 3 and 4` (e.g. `object-deployment-docker-build`)
3. Click on `Start build` button found in detail CodeBuild project view.
4. You will be redirected to the build confirmation screen (you can override the default values provided for the Environment Variables in `Step 3` just before build if its required during this screen)
5. Click on `Start build` button to trigger the build.
6. Upon successful built you should be able to see the docker image created/updated in the AWS ECR repository. Also you can view build history/logs/status via detail CodeBuild project view mentioned `3`.


### Contribution guidelines ###

* You can change this docker image in order to reflect with any local machine environment related changes in the object/tomcat setup. 
